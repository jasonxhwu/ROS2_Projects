#include "rclcpp/rclcpp.hpp"
#include "example_interfaces/msg/int64.hpp"
 
class numberPlusherNode : public rclcpp::Node // MODIFY NAME
{
public:
    numberPlusherNode() : Node("number_plusher") // MODIFY NAME
    {
        this->declare_parameter("starting_number", 2);
        RCLCPP_INFO(this->get_logger(), "%d", this->get_parameter("starting_number").as_int());
        number = this->get_parameter("starting_number").as_int();
        publisher_ = this->create_publisher<example_interfaces::msg::Int64>("number", 10);
        timer_ = this->create_wall_timer(std::chrono::milliseconds(500),
                                         std::bind(&numberPlusherNode::publishNum, this));
        RCLCPP_INFO(this->get_logger(), "Number Plusher has been started!");
    }
 
private:
    rclcpp::Publisher<example_interfaces::msg::Int64>::SharedPtr publisher_;
    int64_t number;
    rclcpp::TimerBase::SharedPtr timer_;

    void publishNum() 
    {
        auto msg = example_interfaces::msg::Int64();
        msg.data = number;
        publisher_->publish(msg);
    }
};
 
int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    auto node = std::make_shared<numberPlusherNode>();
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}