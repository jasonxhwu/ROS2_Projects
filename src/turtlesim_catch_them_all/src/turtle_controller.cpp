#include "rclcpp/rclcpp.hpp"
#include "turtlesim/msg/pose.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "my_robot_interfaces/msg/turtle.hpp"
#include "my_robot_interfaces/msg/turtle_array.hpp"
#include "my_robot_interfaces/srv/catch_turtle.hpp"
#include <cmath>


class TurtleController : public rclcpp::Node
{
public:
    TurtleController() : Node("turtle_controller"), got_pose(false)
    {
        publisher_ = this->create_publisher<geometry_msgs::msg::Twist>("turtle1/cmd_vel", 10);
        
        subscriber_ = this->create_subscription<turtlesim::msg::Pose>(
            "turtle1/pose", 10, std::bind(&TurtleController::callback, this, std::placeholders::_1));     
        turtles_subscriber = this->create_subscription<my_robot_interfaces::msg::TurtleArray>(
            "turtles_alive", 10, std::bind(&TurtleController::callback_turtles, this, std::placeholders::_1));

        timer_ = this->create_wall_timer(
            std::chrono::milliseconds(100), std::bind(&TurtleController::control_loop, this));
        RCLCPP_INFO(this->get_logger(), "Callback successful");
    }

private:
    bool got_pose;
    turtlesim::msg::Pose pose_;
    my_robot_interfaces::msg::Turtle turtle_;
    rclcpp::Subscription<turtlesim::msg::Pose>::SharedPtr subscriber_;
    rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_;
    rclcpp::Subscription<my_robot_interfaces::msg::TurtleArray>::SharedPtr turtles_subscriber;
    rclcpp::TimerBase::SharedPtr timer_;

    std::vector<std::shared_ptr<std::thread>> kill_turtle_threads;


    void callback(const turtlesim::msg::Pose::SharedPtr pose)
    {
        pose_ = *pose.get();
        got_pose = true;
    }

    void callback_turtles(const my_robot_interfaces::msg::TurtleArray::SharedPtr msg)
    {
        if (!msg->turtles.empty())
        {
            turtle_ = msg->turtles.at(0);
        }
    }

    void control_loop()
    {
        if (!got_pose)
        {
            return;
        }

        auto msg = geometry_msgs::msg::Twist();

        double x_dist = turtle_.x - pose_.x;
        double y_dist = turtle_.y - pose_.y;
        double hypoteneus = std::sqrt(x_dist*x_dist + y_dist*y_dist);
        RCLCPP_INFO(this->get_logger(), "distance: %f", hypoteneus);
        
        if (hypoteneus > 0.5)
        {
            // Position
            msg.linear.x = 2 * hypoteneus;

            // Angle
            double angle = std::atan2(y_dist, x_dist) - pose_.theta;
            if (angle > M_PI)
            {
                angle -= 2 * M_PI;
            }
            else if (angle < -1 * M_PI)
            {
                angle += 2 * M_PI;
            }
            msg.angular.z = 6 * angle;
        }
        else
        {
            msg.linear.x = 0.0;
            msg.angular.z = 0.0;
            kill_turtle_threads.push_back(
                std::make_shared<std::thread>(
                    std::bind(&TurtleController::call_kill_turtle, this, turtle_.name)
                )
            );
            turtle_.name = "";
        }
        publisher_->publish(msg);
    }

    void call_kill_turtle(std::string name)
    {
        auto client = this->create_client<my_robot_interfaces::srv::CatchTurtle>("catch_turtle");
        while (!client->wait_for_service(std::chrono::seconds(1)))
        {
            RCLCPP_WARN(this->get_logger(), "Waiting for Service Server to be up...");
        }

        auto request = std::make_shared<my_robot_interfaces::srv::CatchTurtle::Request>();
        request->name = name;

        auto future = client->async_send_request(request);

        try
        {
            auto response = future.get();
            if (!response->success)
            {
                RCLCPP_ERROR(this->get_logger(), "Failed to catch turtle");
            }
        }
        catch (const std::exception &e)
        {
            RCLCPP_ERROR(this->get_logger(), "Service call failed.");
        }
    }
};

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    auto node = std::make_shared<TurtleController>();
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}