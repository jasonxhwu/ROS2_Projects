#include "rclcpp/rclcpp.hpp"
#include "turtlesim/srv/spawn.hpp"
#include "turtlesim/srv/kill.hpp"
#include "my_robot_interfaces/msg/turtle.hpp"
#include "my_robot_interfaces/msg/turtle_array.hpp"
#include "my_robot_interfaces/srv/catch_turtle.hpp"
#include <time.h>
#include <cmath>

class TurtleSpawner : public rclcpp::Node
{

public:
    TurtleSpawner() : Node("turtle_spawner"), count(0)
    {
        timer_ = this->create_wall_timer(
            std::chrono::seconds(1),
            std::bind(&TurtleSpawner::spawn_turtle, this));
        publisher_ = this->create_publisher<my_robot_interfaces::msg::TurtleArray>("turtles_alive", 10);
        kill_service_ = this->create_service<my_robot_interfaces::srv::CatchTurtle>(
            "catch_turtle", std::bind(&TurtleSpawner::catch_turtle_callback, this, std::placeholders::_1, std::placeholders::_2)
        );
    }

private:
    // Initialized instances
    int count;
    rclcpp::TimerBase::SharedPtr timer_;
    std::vector<std::shared_ptr<std::thread>> alive_threads_;
    std::vector<std::shared_ptr<std::thread>> kill_threads_;
    std::vector<my_robot_interfaces::msg::Turtle> turtles_;
    rclcpp::Publisher<my_robot_interfaces::msg::TurtleArray>::SharedPtr publisher_;
    rclcpp::Service<my_robot_interfaces::srv::CatchTurtle>::SharedPtr kill_service_;

    double get_rand()
    {
        return (rand() / (double)RAND_MAX);
    }

    void spawn_turtle()
    {
        if ((int) turtles_.size() < 4) {
            count++;
            auto name = "t-" + std::to_string(count);
            double x = get_rand() * 10;
            double y = get_rand() * 10;
            RCLCPP_INFO(this->get_logger(), "%f", x);
            RCLCPP_INFO(this->get_logger(   ), "%f", y);

            double theta = get_rand() * 6 * M_PI;
            alive_threads_.push_back(
                std::make_shared<std::thread>(std::bind(&TurtleSpawner::call_spawn_turtle, this, x, y, theta)));
        }
        
    }

    void publish_turtles()
    {
        auto msg = my_robot_interfaces::msg::TurtleArray();
        msg.turtles = turtles_;
        publisher_->publish(msg);
    }

    void call_spawn_turtle(double x, double y, double theta)
    {
        auto client = this->create_client<turtlesim::srv::Spawn>("spawn");
        while (!client->wait_for_service(std::chrono::seconds(1)))
        {
            RCLCPP_WARN(this->get_logger(), "Waiting for Service Server to be up...");
        }
        auto request = std::make_shared<turtlesim::srv::Spawn::Request>();

        request->x = x;
        request->y = y;
        request->theta = theta;

        auto future = client->async_send_request(request);
        try
        {
            auto response = future.get();
            if (response->name != "")
            {
                RCLCPP_INFO(this->get_logger(), "Turtle %s is now alive.", response->name.c_str());
                auto t = my_robot_interfaces::msg::Turtle();
                t.name = response->name;
                t.x = x;
                t.y = y;
                t.theta = theta;
                turtles_.push_back(t);
                publish_turtles();
            }
        }
        catch (const std::exception &e)
        {
            RCLCPP_ERROR(this->get_logger(), "Service call failed.");
        }
    }
    void catch_turtle_callback(const my_robot_interfaces::srv::CatchTurtle::Request::SharedPtr request,
                               const my_robot_interfaces::srv::CatchTurtle::Response::SharedPtr response)
    {
        kill_threads_.push_back(
            std::make_shared<std::thread>(std::bind(&TurtleSpawner::kill_turtle, this, request->name)));
        response->success = true;

    }

    void kill_turtle(std::string name)
    {
        auto client = this->create_client<turtlesim::srv::Kill>("kill");
        while (!client->wait_for_service(std::chrono::seconds(1)))
        {
            RCLCPP_WARN(this->get_logger(), "Waiting for Service Server to be up...");
        }

        auto request = std::make_shared<turtlesim::srv::Kill::Request>();
        request->name = name;

        auto future = client->async_send_request(request);
        try
        {
            future.get();
            for (int i = 0; i < (int)turtles_.size(); i++)
            {
                if (turtles_.at(i).name == name)
                {
                    turtles_.erase(turtles_.begin() + i);
                    RCLCPP_INFO(this->get_logger(), "A turtle died");

                    publish_turtles();
                    break;
                }
            }
        }
        catch (const std::exception &e)
        {
            RCLCPP_ERROR(this->get_logger(), "Service call failed.");
        }
    }
};

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    auto node = std::make_shared<TurtleSpawner>(); // MODIFY NAME
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}