#include "rclcpp/rclcpp.hpp"
#include "example_interfaces/msg/int64.hpp"
#include "example_interfaces/srv/set_bool.hpp"

class numberCounterNode : public rclcpp::Node // MODIFY NAME
{
public:
    numberCounterNode() : Node("number_counter") // MODIFY NAME
    {
        counter = 0;
        publisher_ = this->create_publisher<example_interfaces::msg::Int64>("number_count", 10);
        subscriber_ = this->create_subscription<example_interfaces::msg::Int64>(
            "number", 10,
            std::bind(&numberCounterNode::callbackPlusher, this, std::placeholders::_1));
        timer_ = this->create_wall_timer(std::chrono::milliseconds(500),
                                         std::bind(&numberCounterNode::publishNum, this));
        RCLCPP_INFO(this->get_logger(), "Number Counter has started!");
        
        server_ = this->create_service<example_interfaces::srv::SetBool>(
            "reset_counter",
            std::bind(&numberCounterNode::clientServerCallback, this, std::placeholders::_1, std::placeholders::_2));
        RCLCPP_INFO(this->get_logger(), "Server has started!");
    }

private:
    rclcpp::Publisher<example_interfaces::msg::Int64>::SharedPtr publisher_;
    rclcpp::Subscription<example_interfaces::msg::Int64>::SharedPtr subscriber_;
    rclcpp::TimerBase::SharedPtr timer_;
    int64_t counter;

    void callbackPlusher(const example_interfaces::msg::Int64::SharedPtr msg)
    {
        counter += msg->data;
        RCLCPP_INFO(this->get_logger(), "%d", counter);
    }

    void publishNum()
    {
        auto msg = example_interfaces::msg::Int64();
        msg.data = counter;
        publisher_->publish(msg);
    }

    rclcpp::Service<example_interfaces::srv::SetBool>::SharedPtr server_;
    void clientServerCallback(const example_interfaces::srv::SetBool::Request::SharedPtr request,
                              const example_interfaces::srv::SetBool::Response::SharedPtr response)
    {
        if (request->data) 
        {
            counter = 0;
            response->success = true;
            response->message = "Counter has been reset";
        }
        else 
        {
            response->message = "Counter has not been reset";
        }
        
    }
};

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    auto node = std::make_shared<numberCounterNode>(); // MODIFY NAME
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}