#include "rclcpp/rclcpp.hpp"
#include "my_robot_interfaces/srv/set_led.hpp"
 
class BatteryNode : public rclcpp::Node // MODIFY NAME
{
public:
    BatteryNode() : Node("battery_node") // MODIFY NAME
    {
        prev_time = this->get_clock()->now().seconds();
        timer_ = this->create_wall_timer( 
            std::chrono::milliseconds(10), std::bind(&BatteryNode::updateBattery, this)
        );

    }
    
private:
    int64_t number;
    int64_t battery_state;
    double prev_time;
    rclcpp::TimerBase::SharedPtr timer_;
    std::vector<std::thread> threads_;

    void threadedSetLed(int led, int state) {
        threads_.push_back(std::thread(std::bind(&BatteryNode::callService, this, led, state)));
    }

    void callService(int led, int state) {
        auto client = this->create_client<my_robot_interfaces::srv::SetLed>("set_led");
        while (!client->wait_for_service(std::chrono::seconds(1))) 
        {
            RCLCPP_WARN(this->get_logger(), "Waiting for the server to be up...");
        }
        auto request = std::make_shared<my_robot_interfaces::srv::SetLed::Request>();
        request->led_number = led;
        request->state = state;

        auto future = client->async_send_request(request);
        try
        {
            auto response = future.get();
        }
        catch(const std::exception& e)
        {
            RCLCPP_ERROR(this->get_logger(), "Service call failed");
        }
    }

    void updateBattery() {
        double time = this->get_clock()->now().seconds();
        if (battery_state == 1) {
            if (time - prev_time >= 4) {
                RCLCPP_INFO(this->get_logger(), "Battery is empty! Charging battery...");
                battery_state = 0;
                prev_time = time;
                threadedSetLed(3, 1);
            }
        }
        else if (battery_state == 0) {
            if (time - prev_time >= 6) {
                RCLCPP_INFO(this->get_logger(), "Battery is now full again.");
                battery_state = 1;
                prev_time = time;
                threadedSetLed(3, 0);
            }
        }
    }

};
 
int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    auto node = std::make_shared<BatteryNode>(); // MODIFY NAME
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}