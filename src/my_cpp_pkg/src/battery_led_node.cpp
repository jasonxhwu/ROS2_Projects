#include "rclcpp/rclcpp.hpp"
#include "my_robot_interfaces/msg/led_states.hpp"
#include "my_robot_interfaces/srv/set_led.hpp"

class LedNode : public rclcpp::Node // MODIFY NAME
{
public:
    LedNode() : Node("led_panel")// MODIFY NAME
    {
        this->declare_parameter("states", std::vector<int64_t>{0, 0, 0});
        led_states_ = this->get_parameter("states").as_integer_array();
        
        publisher_ = this->create_publisher<my_robot_interfaces::msg::LedStates>("led_states", 10);
        timer_ = this->create_wall_timer(std::chrono::seconds(4),
                                         std::bind(&LedNode::publishState, this));
        RCLCPP_INFO(this->get_logger(), "Led Node has started");
        service_ = this->create_service<my_robot_interfaces::srv::SetLed>(
            "set_led", std::bind(&LedNode::callback, this, std::placeholders::_1, std::placeholders::_2));
        RCLCPP_INFO(this->get_logger(), "Led Node server has started");
    }

private:
    std::vector<int64_t> led_states_;
    rclcpp::TimerBase::SharedPtr timer_;

    rclcpp::Publisher<my_robot_interfaces::msg::LedStates>::SharedPtr publisher_;
    rclcpp::Service<my_robot_interfaces::srv::SetLed>::SharedPtr service_;

    void publishState()
    {
        auto msg = my_robot_interfaces::msg::LedStates();
        msg.led_states = led_states_;
        publisher_->publish(msg);
    }

    void callback(const my_robot_interfaces::srv::SetLed::Request::SharedPtr request,
                  const my_robot_interfaces::srv::SetLed::Response::SharedPtr response)
    {
        int64_t num = request->led_number;
        int64_t state = request->state;

        if (num > 3 || num < 1)
        {
            response->success = false;
            return;
        }
        else if (state != 1 && state != 0)
        {
            response->success = false;
            return;
        }

        led_states_.at(num - 1) = state;
        response->success = true;
        publishState();
    }
};

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    auto node = std::make_shared<LedNode>(); // MODIFY NAME
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}